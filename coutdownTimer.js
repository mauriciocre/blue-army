////////////////////// TELA DE INSTRUÇÃO DESAPARECE///////////////////////
let instructions = document.getElementById("instrucoes");
instructions.addEventListener('click', closed);

function closed() {
    instructions.style.visibility = "hidden";
    backgroundEffects()
    play()
}




///////////////////////// CRIAR EFEITOS SONOROS///////////////////////////
let victorySoundP1 = document.getElementById("sound3");
let victorySoundP2 = document.getElementById("sound4");
let backgroundMusic = document.getElementById("sound1");
let begin = document.getElementById("sound6");

function victoryTuneP1() {
    victorySoundP1.play()
}

function victoryTuneP2() {
    victorySoundP2.play()
}

function backgroundEffects() {
    backgroundMusic.play()
}

function play() {
    begin.play()
}

////////////////////////// CONTADOR DO JOGADOR 1 ///////////////////////
let segundosRestantes = 59;
let intervalo;
let winP1 = document.getElementById("vitoriaP1");
let winP2 = document.getElementById("vitoriaP2");

function tickTackP1() {

    // Captura a DIV 'timer1' //
    let Display = document.getElementById("timer1");

    //  transforma segundos em mm:ss //
    let min = Math.floor(segundosRestantes / 60);
    let sec = segundosRestantes - (min * 60);

    // Adiciona um zero se os segundos forem inferior a 10//
    if (sec < 10) {
        sec = "0" + sec;
    }
    // concatena minutos e segundos //
    var message = min + ":" + sec;

    // Imprime o display de tempo na tela//
    Display.innerHTML = message;

    // Para se o contador se o tempo chegar a zero//
    if (segundosRestantes === 0) {
        winP2.style.visibility = "visible";
        clearInterval(intervalo);
        clearInterval(intervaloP2);
        victoryTuneP1()
    }

    // Subtrai dos segundos restantes//
    segundosRestantes--;
}


////////////////////////// CONTADOR DO JOGADOR 2 //////////////////////
let segundosRestantesP2 = 59;
let intervaloP2;


function tickTackP2() {

    // Captura a DIV 'timer1' //
    var Display = document.getElementById("timer2");

    //  transforma segundos em mm:ss //
    var min = Math.floor(segundosRestantesP2 / 60);
    var sec = segundosRestantesP2 - (min * 60);

    // Adiciona um zero se os segundos forem inferior a 10//
    if (sec < 10) {
        sec = "0" + sec;
    }
    // concatena minutos e segundos //
    var message = min + ":" + sec;

    // Imprime o display de tempo na tela//
    Display.innerHTML = message;

    // Para se o contador se o tempo chegar a zero//
    if (segundosRestantesP2 === 0) {
        winP1.style.visibility = "visible";
        clearInterval(intervalo);
        clearInterval(intervaloP2);
        victoryTuneP2()
    }

    // Subtrai dos segundos restantes//
    segundosRestantesP2--;
}


///////////////////////////////// TURNOS //////////////////////////////
let Timer1Player = document.getElementById("timer1")
let Timer2Player = document.getElementById("timer2")

function turn1Player() {
    Timer1Player.style.animation = 'brightBorderTurnBlue 0.2s infinite';
    Timer2Player.style.animation = '';
}

function turn2Player() {
    Timer1Player.style.animation = '';
    Timer2Player.style.animation = 'brightBorderTurnRed 0.2s infinite';
}