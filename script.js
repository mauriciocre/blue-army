// CRIA AS LINHAS E COLUNAS DO JOGO
function linhas() {
    for (let count = 1; count <= 7; count++) {
        let selecionarColuna = document.getElementById("coluna" + count);
        for (let count = 1; count <= 6; count++) {
            let linha = document.createElement("div");
            linha.id = "linha" + count;
            linha.className = "linhas";
            selecionarColuna.appendChild(linha);
        }
    }
}
linhas();


// MAPA PARA INVERTIDO DO TABULEIRO, VAMOS COLOCAR PRETO OU VERMELHO NO LUGAR DOS ZEROS
let map = [
    // LINHA1 , LINHA 2
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0], //COLUNA 1
    [0, 0, 0, 0, 0, 0, 0], //COLUNA 2
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
];

// ARRAY DAS LINHAS  DE CADA COLUNA

let arrayLinhas = [0, 1, 1, 1, 1, 1, 1, 1, 0, 0];


// CONTADOR DE JOGADAS, SE FOR PAR = VERMELHO/ IMPAR = PRETO
countJogadas = 0;

// FUNÇÃO QUE QUANDO CHAMAR AO CLICAR NA COLUNA, INSERE UM DISCO
function criarDisco(coluna) {
    if (arrayLinhas[coluna] >= 7) {
        return;
    }
    let disco = document.createElement("div");
    disco.id = "disco";
    if (verificarJogador(countJogadas) == "vermelho") {
        let cor = "vermelho"
        disco.className = cor;
        document.querySelector("#coluna" + coluna + " #linha" + arrayLinhas[coluna]).appendChild(disco);
        disco.style.backgroundImage = "url(./img/esfera_vermelha_animada.png)";
        map[coluna][arrayLinhas[coluna]] = cor

        // VERIFICA SE ENCAIXA ALGUMA CONDICACAO DE VITORIA NAQUELE MOVIMENTO
        vitoriaVertical(coluna, cor);
        vitoriaHorizontal(coluna, cor);
        vitoriaDiagonal(coluna, cor);
        countJogadas++;
        arrayLinhas[coluna]++;

        clearInterval(intervaloP2)
        turn1Player()
        intervalo = setInterval(tickTackP1, 1000);

        return;
    }
    if (verificarJogador(countJogadas) == "preto") {
        let cor = "preto"
        disco.className = "preto";
        document.querySelector("#coluna" + coluna + " #linha" + arrayLinhas[coluna]).appendChild(disco);
        disco.style.backgroundImage = "url(./img/esfera_preta_animada.png)";
        map[coluna][arrayLinhas[coluna]] = cor;

        // VERIFICA SE ENCAIXA ALGUMA CONDICACAO DE VITORIA NAQUELE MOVIMENTO
        vitoriaVertical(coluna, cor);
        vitoriaHorizontal(coluna, cor);
        vitoriaDiagonal(coluna, cor);
        countJogadas++;
        arrayLinhas[coluna]++;

        clearInterval(intervalo)
        turn2Player()
        intervaloP2 = setInterval(tickTackP2, 1000);

        return;
    }

}

// VERIFICA DE QUEM É A VEZ
function verificarJogador(countJogadas) {
    if (countJogadas % 2 == 0) { //par
        return "vermelho";
    }
    if (countJogadas % 2 != 0) { //impar
        return "preto";
    }
}